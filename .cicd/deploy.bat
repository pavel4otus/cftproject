for /f %%i in ('jq-win64 .data.db_user .cicd/q_file') do set db_user=%%i
for /f %%i in ('jq-win64 .data.db_password .cicd/q_file') do set db_passw=%%i
echo %db_user% 
echo %db_passw%
eclipsec.exe -clean -nosplash -nl ru_RU -application ru.cft.platform.deployment.bootstrap.Deployment -deploy -server "%test_base%" -owner "IBS" -username %db_user% -pass %db_passw%-projectpath ".\ZIP\patch.zip" -pckpath ".\ZIP\patch.pck" -data ".\WS_TMP" -log ".\ZIP\patch.log" --launcher.suppressErrors
